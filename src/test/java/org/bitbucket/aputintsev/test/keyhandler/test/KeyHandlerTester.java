package org.bitbucket.aputintsev.test.keyhandler.test;

import org.bitbucket.aputintsev.test.keyhandler.Key;
import org.bitbucket.aputintsev.test.keyhandler.KeyHandler;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.*;

public class KeyHandlerTester {

    public static final int MAX_THREADS = 8;
    private KeyHandler keyHandler;
    private ExecutorService executorService = Executors.newFixedThreadPool(MAX_THREADS);

    @Before
    public void init() {
        keyHandler = new KeyHandler(new ExternalSystemImpl());
    }

    @Test
    public void testEqualKeys() {

        Key key = new Key("key1");
        Key sameKey = new Key("key1");

        Future keyTaskResult = executorService.submit(() -> {keyHandler.handle(key); return null;});
        Future sameKeyTaskResult = executorService.submit(() -> {keyHandler.handle(sameKey); return null;});

        try {
            keyTaskResult.get();
            sameKeyTaskResult.get();

            Assert.assertTrue("All keys should be processed eventually, even if some of them are equal.",
                    keyTaskResult.isDone());
            Assert.assertTrue("All keys should be processed eventually, even if some of them are equal.",
                    sameKeyTaskResult.isDone());

        } catch (InterruptedException | ExecutionException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void testNonEqualKeys() {
        Key key1 = new Key("key1");
        Key key2 = new Key("key2");

        Future task1 = executorService.submit(() -> {keyHandler.handle(key1); return null;});
        Future task2 = executorService.submit(() -> {keyHandler.handle(key2); return null;});

        try {
            task1.get();
            task2.get();

            Assert.assertTrue("All keys should be processed eventually, even if some of them are equal.",
                    task1.isDone());
            Assert.assertTrue("All keys should be processed eventually, even if some of them are equal.",
                    task2.isDone());

        } catch (InterruptedException | ExecutionException e) {
            Assert.fail(e.getMessage());
        }
    }
}
