package org.bitbucket.aputintsev.test.keyhandler.test;

import org.bitbucket.aputintsev.test.keyhandler.ExternalSystem;
import org.bitbucket.aputintsev.test.keyhandler.Key;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class ExternalSystemImpl implements ExternalSystem {

    private final Set<Key> processingKeys = ConcurrentHashMap.newKeySet();

    public void process(Key key) {
        if (!processingKeys.add(key))
            throw new IllegalStateException("Trying to process duplicate keys!");

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }

        processingKeys.remove(key);
    }
}
