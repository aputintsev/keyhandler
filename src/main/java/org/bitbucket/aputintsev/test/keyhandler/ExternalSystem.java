package org.bitbucket.aputintsev.test.keyhandler;

public interface ExternalSystem {
    void process(Key key);
}
