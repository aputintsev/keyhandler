package org.bitbucket.aputintsev.test.keyhandler;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class KeyHandler {
    private ExternalSystem externalSystem;

    private final ConcurrentMap<Key, Lock> locksMap = new ConcurrentHashMap<>();

    public void handle(Key key) {
        Lock lock = locksMap.get(key);
        if (lock == null) {
            final Lock newLock = new ReentrantLock();
            lock = locksMap.putIfAbsent(key, newLock);
            if (lock == null) {
                lock = newLock;
            }
        }

        try {
            lock.lock();
            externalSystem.process(key);
        } finally {
            lock.unlock();
        }
    }

    public KeyHandler(ExternalSystem externalSystem) {
        this.externalSystem = externalSystem;
    }

}
