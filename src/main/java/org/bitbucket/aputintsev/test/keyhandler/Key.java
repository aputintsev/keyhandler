package org.bitbucket.aputintsev.test.keyhandler;

import java.util.Objects;

public class Key {
    private String key;

    public Key(String key) {
        this.key = key;
    }

    public boolean equals(Object that) {
        if (this == that) return true;

        if (!(that instanceof Key)) return false;
        return (this.key.equals(((Key) that).getKey()));
    }

    public int hashCode() {
        return Objects.hash(this.key);
    }

    public String getKey() {
        return this.key;
    }
}
